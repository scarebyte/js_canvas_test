
function draw() {
    var canvas = document.getElementById("jcanvas");
    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");

        ctx.fillStyle = "rgb(200,0,0)";
        ctx.fillRect (10, 10, 55, 50);

        ctx.fillStyle = "rgba(0, 0, 200, 0.2)";
        ctx.fillRect (30, 30, 55, 50);
    }
}

$(document).ready(function() {
    draw();

    $("#send-button").click(function() {

    });

    $("#kill-button").click(function() {

    });

    $("#echo-button").click(function() {

    });
});